/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

function TestPerformanceVarArgs(milliseconds, funcToTest)
{
    // The variable arguments version is slightly slower than calling the other
    // performance functions, so the results will be a little more
    // accurate for the other functions.

    var ret;
    var count = 0;
    var start;
    var args = Array.prototype.splice.call(arguments, 2);

    // Test if the function returns a value before commencing the timings.
    if (typeof funcToTest.apply(null, args) === 'undefined')
    {
        start = Date.now();
        do
        {
            funcToTest.apply(null, args);
            count++;
        }
        while (Date.now() - start < milliseconds);
        ret = "";
    }
    else
    {
        // Time the function that returns a value.
        start = Date.now();
        do
        {
            ret = funcToTest.apply(null, args);
            count++;
        }
        while (Date.now() - start < milliseconds);
    }

    var call_time_in_seconds = (milliseconds / count) / 1000;
    var name = funcToTest.name;

    return {
        name: name,
        args: args,
        ret: ret,
        count: count,
        milliseconds: milliseconds,
        call_time_in_seconds: call_time_in_seconds
    };
}

function TestPerformance(milliseconds, funcToTest)
{
    var ret;
    var count = 0;
    var start;
    var args = [];

    // Test if the function returns a value before commencing the timings.
    if (typeof funcToTest() === 'undefined')
    {
        start = Date.now();
        do
        {
            funcToTest();
            count++;
        }
        while (Date.now() - start < milliseconds);
        ret = "";
    }
    else
    {
        // Time the function that returns a value.
        start = Date.now();
        do
        {
            ret = funcToTest();
            count++;
        }
        while (Date.now() - start < milliseconds);
    }

    var call_time_in_seconds = (milliseconds / count) / 1000;
    var name = funcToTest.name;

    return {
        name: name,
        args: args,
        ret: ret,
        count: count,
        milliseconds: milliseconds,
        call_time_in_seconds: call_time_in_seconds
    };
}

function TestPerformanceOneArg(milliseconds, funcToTest, arg)
{
    var ret;
    var count = 0;
    var start;
    var args = [arg];

    // Test if the function returns a value before commencing the timings.
    if (typeof funcToTest(arg) === 'undefined')
    {
        start = Date.now();
        do
        {
            funcToTest(arg);
            count++;
        }
        while (Date.now() - start < milliseconds);
        ret = "";
    }
    else
    {
        // Time the function that returns a value.
        start = Date.now();
        do
        {
            ret = funcToTest(arg);
            count++;
        }
        while (Date.now() - start < milliseconds);
    }

    var call_time_in_seconds = (milliseconds / count) / 1000;
    var name = funcToTest.name;

    return {
        name: name,
        args: args,
        ret: ret,
        count: count,
        milliseconds: milliseconds,
        call_time_in_seconds: call_time_in_seconds
    };
}

function TestPerformanceTwoArgs(milliseconds, funcToTest, arg1, arg2)
{
    var ret;
    var count = 0;
    var start;
    var args = [arg1, arg2];

    // Test if the function returns a value before commencing the timings.
    if (typeof funcToTest(arg1, arg2) === 'undefined')
    {
        start = Date.now();
        do
        {
            funcToTest(arg1, arg2);
            count++;
        }
        while (Date.now() - start < milliseconds);
        ret = "";
    }
    else
    {
        // Time the function that returns a value.
        start = Date.now();
        do
        {
            ret = funcToTest(arg1, arg2);
            count++;
        }
        while (Date.now() - start < milliseconds);
    }

    var call_time_in_seconds = (milliseconds / count) / 1000;
    var name = funcToTest.name;

    return {
        name: name,
        args: args,
        ret: ret,
        count: count,
        milliseconds: milliseconds,
        call_time_in_seconds: call_time_in_seconds
    };
}

function TestResultsToString(res)
{
    var args_str = "";

    for (var i = 0; i < res.args.length; i++)
    {
        if ((typeof res.args[i] === 'string') || (res.args[i] instanceof String))
            args_str += "\"" + res.args[i] + "\",";
        else
            args_str += res.args[i] + ",";
    }
    if (args_str.length > 0)
        args_str = args_str.substr(0, args_str.length - 1);

    var ret = res.name + "(" + args_str + ")";
    ret += " returned [" + res.ret + "]";
    ret += " with " + res.count + " calls";
    ret += " in " + res.milliseconds + " milliseconds";
    ret += " or " + res.call_time_in_seconds.toFixed(10) + " seconds per call";

    return ret;
}
